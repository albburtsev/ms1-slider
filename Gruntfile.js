module.exports = function(grunt) {
	'use strict';

	require('matchdep')
		.filterDev('grunt-*')
		.forEach(grunt.loadNpmTasks);

	grunt.initConfig({
		jshint: {
			options: {
				jshintrc: '.jshintrc'
			},
			src: ['jquery.ms1-slider.js']
		},

		uglify: {
			slider: {
				files: {
					'jquery.ms1-slider.min.js': ['jquery.ms1-slider.js']
				}
			}
		},

		watch: {
			slider: {
				files: ['jquery.ms1-slider.js'],
				tasks: ['jshint', 'uglify']
			}
		}
	});

	grunt.registerTask('default', ['jshint', 'uglify', 'watch']);
}
