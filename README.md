# jquery.ms1-slider

## Опции

Опции добавляются в качестве data-атрибутов родительского контейнера ```.ms1slider```

 * **autoplay** – автопрокрутка слайдов с интервалом ```delay```, по умолчанию – ```true```;
 * **delay** – время (ms) между автопролистыванием слайдов, по умолчанию – ```7000```;
 * **duration** – продолжительность (ms) анимации пролистывания, по умолчанию – ```500```;
 * **points** – показывать блок "точек", по умолчанию – ```true```;
 * **ears** – показывать кликабельные контролы "уши", по умолчанию – ```true```.

Пример отключения "точек" и ускорения анимации пролистывания:

```html
<div class="ms1slider" data-points="false" data-duration="200">
```

## Swipe

Поддержка swipe реализована для jQuery-плагина [jquery.touchSwipe.js](https://github.com/mattbryson/TouchSwipe-Jquery-Plugin)
Необходимо просто подключить этот плагин на страницу и во всех слайдерах заработает swipe.

## Код

### HTML

```html
<div class="ms1slider">
    <div class="ms1slider__slide">
        <p>#1</p>
    </div>
    <div class="ms1slider__slide">
        <p>#2</p>
    </div>
    <div class="ms1slider__slide">
        <p>#3</p>
    </div>
    <div class="ms1slider__slide">
        <p>#4</p>
    </div>
    <div class="ms1slider__slide">
        <p>#5</p>
    </div>
</div>
```

### Стили

```css
.ms1slider {
	position: relative;
	overflow: hidden;
	width: 1200px;
	height: 300px;
	}

	.ms1slider__container {
		position: relative;
		height: 100%;
		}

	.ms1slider__slide {
		position: absolute;
		z-index: 1;
		top: 0;
		width: 100%;
		height: 100%;
		}

	.ms1slider__points {
		position: absolute;
		z-index: 10;
		bottom: 10px;
		left: 50%;
		text-align: center;
		}

		.ms1slider__point {
			display: inline-block;
			width: 4px;
			height: 4px;
			margin-right: 10px;
			background: #FFF;
			}

			.ms1slider__point.selected {
				background: #000;
				}

	.ms1slider__ears {
		position: absolute;
		top: 0;
		z-index: 10;
		width: 30px;
		height: 100%;
		background: rgba(255, 255, 255, 0.5);
		cursor: pointer;
		}

		.ms1slider__ears_left {
			left: 0;
			}

		.ms1slider__ears_right {
			right: 0;
			}
```