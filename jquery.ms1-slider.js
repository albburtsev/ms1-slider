jQuery(function($) {
	'use strict';

	$.fn.ms1slider = function(opts) {
		var globalOpts = $.extend({
			delay: 7000, // ms
			duration: 500,
			autoplay: true,
			points: true,
			ears: true
		}, opts || {});

		return $(this).each(function() {
			var classBlock = 'ms1slider',
				classSlide = classBlock + '__slide',
				classPoints = classBlock + '__points',
				classPoint = classBlock + '__point',
				classEars = classBlock + '__ears',
				classEarsLeft = classEars + '_left',
				classEarsRight = classEars + '_right',
				classSelected = 'selected';

			var _this = $(this),
				_inner = $('<div>'),
				_points = $('<div>').addClass(classPoints), _pointsItems,
				_earLeft = $('<div>').addClass(classEars + ' ' + classEarsLeft).data('dir', -1),
				_earRight = $('<div>').addClass(classEars + ' ' + classEarsRight).data('dir', 1),
				_ears = _earLeft.add(_earRight),
				_slides = $('.' + classSlide, _this);

			var opts = $.extend({}, globalOpts, _this.data()),
				width = _this.width(),
				length = _slides.length,
				timer = null,
				position = 0,
				isAnimated = false;

			// Sets inner container styles
			_inner
				.css({
					position: 'relative',
					width: width * length,
					height: _this.height()
				})
				.appendTo(_this);

			// Wraps all slides in container
			_slides.each(function(idx) {
				$('<span>')
					.addClass(classPoint)
					.appendTo(_points);

				$(this)
					.css({
						left: width * idx,
						width: width
					})
					.appendTo(_inner);
			});

			// Adds points
			if ( opts.points ) {
				_pointsItems = $('.' + classPoint, _points);
				_pointsItems
					.eq(0)
					.addClass(classSelected);
				_this.append(_points);
			}

			// Adds ears
			if ( opts.ears ) {
				_ears.appendTo(_this);
			}

			// Auto scroll
			if ( opts.autoplay ) {
				timer = setInterval(function() {
					_earRight.trigger('shownext');
				}, opts.delay);
			}

			// Slides animation
			_ears.bind('click shownext', function(e) {
				if ( isAnimated ) {
					return;
				}

				if ( e.type === 'click' ) {
					clearInterval(timer);
				}

				var	dir = $(this).data('dir');

				position += dir;
				isAnimated = true;
				_slides = $('.' + classSlide, _this);

				if ( _pointsItems ) {
					_pointsItems
						.removeClass(classSelected)
						.eq(position % length)
						.addClass(classSelected);
				}

				if ( dir < 0 ) {
					_slides
						.last()
						.css('left', position * width)
						.prependTo(_inner);
				}

				_inner.animate({
					marginLeft: -width * position
				}, opts.duration, 'swing', function() {
					if ( dir > 0 ) {
						_slides
							.first()
							.css('left', (length + position - 1) * width)
							.appendTo(_inner);
					}
					isAnimated = false;
				});
			});

			// Swipe support
			if ( _this.swipe ) {
				_this.swipe({
					triggerOnTouchEnd : true,
					allowPageScroll: 'vertical',
					swipe: function(e, dir) {
						if ( dir === 'left' ) {
							_earRight.triggerHandler('click');
						} else if ( dir === 'right' ) {
							_earLeft.triggerHandler('click');
						}
					}
				});
			}
		});
	};

	// Auto init
	$('.ms1slider').ms1slider();
});
